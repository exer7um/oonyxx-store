import Vue from "vue";
import firebase from "firebase";
import { firestorePlugin } from "vuefire";

Vue.use(firestorePlugin);

const firebaseConfig = {
  apiKey: "AIzaSyDqDCnop15IU7WnfLIZjmNC2EODlJ2ZlKw",
  authDomain: "oonyxx-store.firebaseapp.com",
  databaseURL: "https://oonyxx-store.firebaseio.com",
  projectId: "oonyxx-store",
  storageBucket: "",
  messagingSenderId: "41117801731",
  appId: "1:41117801731:web:d2ce2e3581bdec53"
};
const firebaseApp = firebase.initializeApp(firebaseConfig);

export const db = firebaseApp.firestore();
